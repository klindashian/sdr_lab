### Lesson 17. Frequency Locked Loop (FLL)

В качестве информационного сигнала выступает двоичный сигнал с фазовым сдвигом. Исходная схема представлена на рисунке 1.

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson17/images/lesson17.png" width="60%"/>
    <figcaption><center>Рисунок 1. Исходная схема </center></figcaption>
    </figure>
</div>

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson17/images/lesson17_1.png" width="60%"/>
    <figcaption><center>Рисунок 2. Результат запуска схемы </center></figcaption>
    </figure>
</div>

На первом графике видно, что в передаваемом сигнале содержится только реальная часть. Полоса пропускания избыточна.

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson17/images/lesson17_2_2.jpg" width="60%"/>
    <figcaption><center>Рисунок 3. Результат запуска схемы </center></figcaption>
    </figure>
</div>

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson17/images/lesson17_3.jpg" width="60%"/>
    <figcaption><center>Рисунок 4. Глазковая диаграмма </center></figcaption>
    </figure>
</div>

На глазковой диаграмме видно, что нет дрожания фазы, то есть биты сигнала принимаются корретно,глазковая диаграмма — это суммарный вид всех битовых периодов измеряемого сигнала, наложенных друг на друга.

Влияние смещения частоты (вручную):

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson17/images/lesson17_2_3.jpg" width="60%"/>
    <figcaption><center>Рисунок 4. Результат смещения частоты </center></figcaption>
    </figure>
</div>

Полученный спектр обозначенный красным цветом не центрирован вокруг нулевой частоты. Если мы умножим на синий фильтр и суммируем, мы получим большой отрицательный вклад от левого выступа и практически ничего от правого выступа, поскольку полученный спектр там фактически исчезает. Таким образом, сумма отрицательна, что говорит нам о том, что красная кривая была сдвинута на более низкую частоту по сравнению с желаемым симметричным расположением.
Первым шагом в автоматизации восстановления сигнала является корректировка разницы в тактовой частоте между передатчиком и приемником. Ключевой момент здесь заключается в том, чтобы посмотреть на диапазон частот приемника и настроить его так, чтобы он был сосредоточен вокруг нуля. Для этого настроен блок `FLL Band-Edge`

Для этого мы используем полосовой фильтр с положительным пиком, сосредоточенным на том месте, где правильный спектр падает с положительной стороны, и отрицательным пиком, где спектр падает с отрицательной стороны

На рисунке зеленым обозначен спектр после блока `FLL Band-Edge`:

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson17/images/lesson17_2_4.jpg" width="60%"/>
    <figcaption><center>Рисунок 6. Спектр сигнала </center></figcaption>
    </figure>
</div>

Происходит автоматические центрирование спектра сигнала.

Влияние шума:

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson17/images/lesson17_4.jpg" width="60%"/>
    <figcaption><center>Рисунок 7. Влияние шума </center></figcaption>
    </figure>
</div>


 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson17/images/lesson17_5.jpg" width="60%"/>
    <figcaption><center>Рисунок 8. Дрожание фазы </center></figcaption>
    </figure>
</div>

