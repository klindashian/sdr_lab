### Lesson 11. Amplitude-Shift Keying (ASK) from Pluto to SDR
В примере [lesson10](https://gitlab.com/klindashian/sdr_lab/-/blob/main/Ex_GNURadio/Lesson10_On-off_Keying.md) используется двухпозиционная манипуляция для модуляции несущей синусоидальной волны для передачи информации, то есть использовались два разных уровня сигнала: 0 и 1. В данной модели более двух уровней амплитуды.

 В качестве входных значений задан вектор (0, 1/3, 2/3, 1). Каждый раз, когда передается новый символ, это будет одно из этих четырех значений (символов). Итак, мы можем передавать два бита за раз. Исходный вид модели представлен на рисунке:

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson11/images/lesson11_1.png" width="60%"/>
    <figcaption><center>Рисунок 1. Исходная модель </center></figcaption>
    </figure>
</div>

Результат запуска схемы:

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson11/images/lesson11_2.png" width="60%"/>
    <figcaption><center>Рисунок 2.1. Графики зависимости амплитуды от времени </center></figcaption>
    </figure>
</div>

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson11/images/lesson11_3.png" width="60%"/>
    <figcaption><center>Рисунок 2.2. Спектр сигнала </center></figcaption>
    </figure>
</div>

На нижнем графике привидено сигнальное созвездие, видно, что точки на нем соответсвуют нашей заданной последовательности, так как в канале нет шума и поворота фазы.

Для визуализации поворота фазы схема была изменена, как показано на рисунке 3:

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson11/images/lesson11_5.png" width="60%"/>
    <figcaption><center>Рисунок 3. Добавление блока фазового сдвига </center></figcaption>
    </figure>
</div>

Результат:

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson11/images/lesson11_4.png" width="60%"/>
    <figcaption><center>Рисунок 4. Поворот фазы </center></figcaption>
    </figure>
</div>


 Поскольку нас волнует только величина получаемых значений, мы отправляем выходные данные в блок Complex to Mag для извлечения амплитуды, а также в блок Keep 1 in N, чтобы извлечь только одну выборку для каждого отправляемого значения sps. Это должно обеспечить точное представление четырех величин, которые кодируют символы.

Также в примере приведена схема декодирования сигнала.

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson11/images/lesson11_decode.png" width="60%"/>
    <figcaption><center>Рисунок 5. Блок схема </center></figcaption>
    </figure>
</div>

Выводы результатов работы схемы:

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson11/images/lesson11_decode_1.png" width="60%"/>
    <figcaption><center>Рисунок 5.1. Визуализация принятого сигнала </center></figcaption>
    </figure>
</div>

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson11/images/lesson11_decode_2.png" width="60%"/>
    <figcaption><center>Рисунок 5.1. Визуализация принятого сигнала </center></figcaption>
    </figure>
</div>

В качестве дополнения приложена [схема](/Ex_GNURadio/Lesson11/images/mpsk.grc) для построения сигнального созвездия MPSK и визуализация вращения.


 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson11/images/mpsk.png" width="60%"/>
    <figcaption><center>Рисунок 6. Схема </center></figcaption>
    </figure>
</div>

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson11/images/mpsk_1.png" width="60%"/>
    <figcaption><center>Рисунок 7. Принятый сигнал и сигнальное созвездие </center></figcaption>
    </figure>
</div>

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson11/images/mpsk_2.png" width="60%"/>
    <figcaption><center>Рисунок 8. Поворот фазы </center></figcaption>
    </figure>
</div>
