### Lesson 16. Constellation Modulator


Блок "constellation modulator" в GNU Radio выполняет модуляцию цифровых данных на созвездие (constellation) для передачи по каналу связи. Он принимает входные цифровые символы и преобразует их в аналоговые сигналы, которые затем могут быть переданы через канал связи. 

В качестве информационного сигнала используется вектор 0 и 1. В данном примере выбрана BPSK модуляция, где количество выборок на символ равно 2.

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson16/images/lesson16.jpeg" width="60%"/>
    <figcaption><center>Рисунок 1. Исходная схема </center></figcaption>
    </figure>
</div>


Частота дискретизации = 1 MГц
Центральная частота = 915 МГц
16 выборок на символ
В данном случае используется случайный сигнал в качестве информационного.
Блок `constellation modulator` выполняет роль фильтра и построения созвездия.

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson16/images/lesson16_hw.jpeg" width="60%"/>
    <figcaption><center>Рисунок 1. Исходная схема </center></figcaption>
    </figure>
</div>

Соответствие между поступающими данными и комплексными числами, каждое из котороых было заранее присвоено в блоке `constellation object` представлено на рисунке ниже.

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson16/images/lesson16_3.jpeg" width="60%"/>
    <figcaption><center>Рисунок 2. Сигнальное созвездие </center></figcaption>
    </figure>
</div>

Данные, которые мы отправляем в `Constellation Modulator`, должны быть собраны в более эффективном виде, чем последовательность случайных байтов, каждый из которых имеет только один значащий бит (для BPSK). Реализовано  это с помощью блока `Unpacked to Packed`. Вторая сложность заключается в том, что мы должны удерживать амплитуду выходного сигнала в пределах диапазона принятого сигнала, который составляет –1 к 1. Поскольку импульсы RRC могут иметь пики, превышающие величину данных, которые мы пытаемся отправить, нужно будет пропустить выходные данные через блок `Multiply Const``.

На рисунке 3 представлен сигнал до обработки:

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson16/images/lesson16_hw_1.jpeg" width="60%"/>
    <figcaption><center>Рисунок 3. Принятый сигнал </center></figcaption>
    </figure>
</div>

Биты плохо различимы, далее используется обработка сигнала и увеличение мощности. Используется блок автоматической регулировки мощности.

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson16/images/lesson16_2.jpeg" width="60%"/>
    <figcaption><center>Рисунок 4. Обработка сигнала </center></figcaption>
    </figure>
</div>

Глазковая диаграмма сигнала после обработки:

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson16/images/lesson16_hw_or.png" width="60%"/>
    <figcaption><center>Рисунок 5. Глазковая диаграмма и спектр сигнала </center></figcaption>
    </figure>
</div>

Синим обозначен спектр принимаемого сигнала, красным - спектр после блока АРУ. Оба спектра сосредоточены около нуля, то есть не сдвинуты.

Посмотрим, как влияет сдвиг частоты на работу схемы:

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson16/images/lesson16_hw_offset.jpeg" width="60%"/>
    <figcaption><center>Рисунок 6 Влияние сдвига частоты </center></figcaption>
    </figure>
</div>

Можно заметить дрожание фазы и сдвиг спектра.
