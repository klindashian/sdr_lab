### Lesson 13. Quadrate Phase Shift Keying 
Квадратурная фазовая модуляция QPSK (Quadrate Phase Shift Keying) является четырехуровневой фазовой модуляцией (M = 4), при которой фаза ВЧ колебания может принимать четыре различных значения с шагом, равным `pi/2`. В отличие от модуляции BPSK, здесь моделируется комплексная часть сигнала.

Исходная схема реализации представлена на рисунке 1. Исходный сигнал уже состоит из 4 разных символов (от 0 до 3).

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson13/images/lesson13.jpeg" width="60%"/>
    <figcaption><center>Рисунок 1. Исходная схема </center></figcaption>
    </figure>
</div>

Результат работы схемы:

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson13/images/lesson13_1.jpeg" width="60%"/>
    <figcaption><center>Рисунок 2.1. Графики выходного сигнала </center></figcaption>
    </figure>
</div>

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson13/images/lesson13_2.jpeg" width="60%"/>
    <figcaption><center>Рисунок 2.2. Графики выходного сигнала </center></figcaption>
    </figure>
</div>

На втором графике изображен график реальной и мнимой части принятого сигнала. По мере прохождения всех симоволов мнимая и реальная части меняются местами (из 1 в -1), то есть в каждый момент времени передаем 2 бита информации. Также изображен спектр сигнала.

На сигнальном созвездии (рисунок 3) представлено созвездие QPSK без (первый график) и с условием сдвига частоты (второй график).

 <div align="center"
    <figure >
    <img src=" /Ex_GNURadio/Lesson13/images/lesson13_3.jpeg" width="60%"/>
    <figcaption><center>Рисунок 3. Сигнальное созвездие </center></figcaption>
    </figure>
</div>

QPSK имеет приемущество в том, что можно передавать 2 бита информации, что приводит к выиграшу скорости передачи данных.

Далее в схему добавлен шум для визуализации его влияния на модель.

 <div align="center"
    <figure >
    <img src=" /Ex_GNURadio/Lesson13/images/lesson13_4.jpeg" width="60%"/>
    <figcaption><center>Рисунок 4. Добавление шума </center></figcaption>
    </figure>
</div>

Визуализация влияния:

 <div align="center"
    <figure >
    <img src=" /Ex_GNURadio/Lesson13/images/lesson13_5.jpeg" width="60%"/>
    <figcaption><center>Рисунок 5.1. Графики результата воздействия шума </center></figcaption>
    </figure>
</div>

 <div align="center"
    <figure >
    <img src=" /Ex_GNURadio/Lesson13/images/lesson13_6.jpeg" width="60%"/>
    <figcaption><center>Рисунок 5.2. Графики результата воздействия шума </center></figcaption>
    </figure>
</div>

Видно, как шум влияет на принятие решения о принятом бите на сигнальном созвездии.