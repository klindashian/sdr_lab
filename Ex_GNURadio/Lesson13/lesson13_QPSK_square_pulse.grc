options:
  parameters:
    author: jason
    catch_exceptions: 'True'
    category: '[GRC Hier Blocks]'
    cmake_opt: ''
    comment: ''
    copyright: ''
    description: ''
    gen_cmake: 'On'
    gen_linking: dynamic
    generate_options: qt_gui
    hier_block_src_path: '.:'
    id: lesson13_QPSK_square_pulse
    max_nouts: '0'
    output_language: python
    placement: (0,0)
    qt_qss_theme: ''
    realtime_scheduling: ''
    run: 'True'
    run_command: '{python} -u {filename}'
    run_options: prompt
    sizing_mode: fixed
    thread_safe_setters: ''
    title: lesson13_QPSK_square_pulse
    window_size: ''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 8]
    rotation: 0
    state: enabled

blocks:
- name: center_freq
  id: variable
  parameters:
    comment: ''
    value: 915e6
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [279, 13]
    rotation: 0
    state: enabled
- name: constellation
  id: variable_constellation
  parameters:
    comment: 'Must know that symbols are secretly and

      automatically normalized so that

      mean(abs(symbols))==1'
    const_points: symbols
    dims: '1'
    normalization: digital.constellation.AMPLITUDE_NORMALIZATION
    precision: '8'
    rot_sym: '4'
    soft_dec_lut: None
    sym_map: '[0, 1,2,3]'
    type: calcdist
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1192, 40.0]
    rotation: 0
    state: true
- name: noise
  id: variable_qtgui_range
  parameters:
    comment: ''
    gui_hint: ''
    label: ''
    min_len: '100'
    orient: QtCore.Qt.Horizontal
    rangeType: float
    start: '0.1'
    step: '0.01'
    stop: '0.5'
    value: '0.1'
    widget: counter
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [240, 620.0]
    rotation: 0
    state: true
- name: offset
  id: variable_qtgui_range
  parameters:
    comment: ''
    gui_hint: ''
    label: ''
    min_len: '200'
    orient: QtCore.Qt.Horizontal
    rangeType: float
    start: -20e3
    step: '1'
    stop: 20e3
    value: +12e3
    widget: counter_slider
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [120, 524.0]
    rotation: 0
    state: true
- name: rx_gain
  id: variable_qtgui_range
  parameters:
    comment: ''
    gui_hint: ''
    label: ''
    min_len: '200'
    orient: QtCore.Qt.Horizontal
    rangeType: float
    start: '0'
    step: '1'
    stop: '70'
    value: '10'
    widget: counter_slider
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [104, 324.0]
    rotation: 0
    state: true
- name: samp_rate
  id: variable
  parameters:
    comment: ''
    value: 1e6
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [184, 12]
    rotation: 0
    state: enabled
- name: sps
  id: variable
  parameters:
    comment: ''
    value: '100'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [648, 28.0]
    rotation: 0
    state: true
- name: symbols
  id: variable
  parameters:
    comment: ''
    value: '[0.4, 0.8, 1.2, 1.6]'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [382, 15]
    rotation: 0
    state: disabled
- name: symbols
  id: variable
  parameters:
    comment: ''
    value: '[1,1j, -1, -1j]'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [379, 77]
    rotation: 0
    state: disabled
- name: symbols
  id: variable
  parameters:
    comment: 'need diagonals to use

      Constellation Decoder'
    value: '[-1-1j, -1+1j, 1+1j, 1-1j]'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [512, 20.0]
    rotation: 0
    state: enabled
- name: tx_attenuation
  id: variable_qtgui_range
  parameters:
    comment: ''
    gui_hint: ''
    label: ''
    min_len: '200'
    orient: QtCore.Qt.Horizontal
    rangeType: float
    start: '0'
    step: '1'
    stop: '89'
    value: '10'
    widget: counter_slider
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [728, 4.0]
    rotation: 0
    state: true
- name: analog_agc_xx_0
  id: analog_agc_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    gain: '1.0'
    max_gain: '65536'
    maxoutbuf: '0'
    minoutbuf: '0'
    rate: 1e-4
    reference: '1.0'
    type: complex
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [744, 180.0]
    rotation: 0
    state: true
- name: analog_noise_source_x_0
  id: analog_noise_source_x
  parameters:
    affinity: ''
    alias: ''
    amp: noise
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    noise_type: analog.GR_GAUSSIAN
    seed: '0'
    type: complex
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [384, 580.0]
    rotation: 0
    state: true
- name: analog_random_uniform_source_x_0
  id: analog_random_uniform_source_x
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maximum: '4'
    maxoutbuf: '0'
    minimum: '0'
    minoutbuf: '0'
    seed: '0'
    type: byte
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [32, 219]
    rotation: 0
    state: disabled
- name: analog_sig_source_x_0
  id: analog_sig_source_x
  parameters:
    affinity: ''
    alias: ''
    amp: '1'
    comment: ''
    freq: offset
    maxoutbuf: '0'
    minoutbuf: '0'
    offset: '0'
    phase: '0'
    samp_rate: samp_rate
    showports: 'False'
    type: complex
    waveform: analog.GR_COS_WAVE
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [208, 424.0]
    rotation: 0
    state: true
- name: blocks_add_xx_0
  id: blocks_add_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_inputs: '2'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [576, 584.0]
    rotation: 0
    state: true
- name: blocks_char_to_float_0
  id: blocks_char_to_float
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    scale: '1'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1272, 764.0]
    rotation: 0
    state: true
- name: blocks_exponentiate_const_cci_0
  id: blocks_exponentiate_const_cci
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    exponent: '4'
    maxoutbuf: '0'
    minoutbuf: '0'
    num_ports: '1'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [936, 204.0]
    rotation: 0
    state: true
- name: blocks_multiply_xx_0
  id: blocks_multiply_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_inputs: '2'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [536, 280.0]
    rotation: 0
    state: true
- name: blocks_repeat_0
  id: blocks_repeat
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    interp: sps
    maxoutbuf: '0'
    minoutbuf: '0'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [576, 124.0]
    rotation: 0
    state: true
- name: blocks_vector_source_x_0
  id: blocks_vector_source_x
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    repeat: 'True'
    tags: '[]'
    type: byte
    vector: '[0,1,2,3,0,3,0,3]'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [72, 132]
    rotation: 0
    state: enabled
- name: digital_chunks_to_symbols_xx_0
  id: digital_chunks_to_symbols_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    dimension: '1'
    in_type: byte
    maxoutbuf: '0'
    minoutbuf: '0'
    num_ports: '1'
    out_type: complex
    symbol_table: symbols
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [336, 152.0]
    rotation: 0
    state: true
- name: digital_constellation_decoder_cb_0
  id: digital_constellation_decoder_cb
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    constellation: constellation
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1064, 772.0]
    rotation: 0
    state: true
- name: digital_costas_loop_cc_0
  id: digital_costas_loop_cc
  parameters:
    affinity: ''
    alias: ''
    comment: 'Loop Bandwidth:

      2*3.14/100'
    maxoutbuf: '0'
    minoutbuf: '0'
    order: '4'
    use_snr: 'False'
    w: 2*3.14/100
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [768, 624.0]
    rotation: 0
    state: true
- name: qtgui_const_sink_x_0_0
  id: qtgui_const_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    axislabels: 'True'
    color1: '"blue"'
    color10: '"red"'
    color2: '"red"'
    color3: '"red"'
    color4: '"red"'
    color5: '"red"'
    color6: '"red"'
    color7: '"red"'
    color8: '"red"'
    color9: '"red"'
    comment: ''
    grid: 'False'
    gui_hint: ''
    label1: Receive Tuned
    label10: ''
    label2: ''
    label3: ''
    label4: ''
    label5: ''
    label6: ''
    label7: ''
    label8: ''
    label9: ''
    legend: 'True'
    marker1: '0'
    marker10: '0'
    marker2: '0'
    marker3: '0'
    marker4: '0'
    marker5: '0'
    marker6: '0'
    marker7: '0'
    marker8: '0'
    marker9: '0'
    name: '""'
    nconnections: '1'
    size: '1024'
    style1: '0'
    style10: '0'
    style2: '0'
    style3: '0'
    style4: '0'
    style5: '0'
    style6: '0'
    style7: '0'
    style8: '0'
    style9: '0'
    tr_chan: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_slope: qtgui.TRIG_SLOPE_POS
    tr_tag: '""'
    type: complex
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    xmax: '2'
    xmin: '-2'
    ymax: '2'
    ymin: '-2'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [944, 484.0]
    rotation: 0
    state: true
- name: qtgui_const_sink_x_0_0_0
  id: qtgui_const_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    axislabels: 'True'
    color1: '"blue"'
    color10: '"red"'
    color2: '"red"'
    color3: '"red"'
    color4: '"red"'
    color5: '"red"'
    color6: '"red"'
    color7: '"red"'
    color8: '"red"'
    color9: '"red"'
    comment: ''
    grid: 'False'
    gui_hint: ''
    label1: Receive Costas
    label10: ''
    label2: ''
    label3: ''
    label4: ''
    label5: ''
    label6: ''
    label7: ''
    label8: ''
    label9: ''
    legend: 'True'
    marker1: '0'
    marker10: '0'
    marker2: '0'
    marker3: '0'
    marker4: '0'
    marker5: '0'
    marker6: '0'
    marker7: '0'
    marker8: '0'
    marker9: '0'
    name: '""'
    nconnections: '1'
    size: '1024'
    style1: '0'
    style10: '0'
    style2: '0'
    style3: '0'
    style4: '0'
    style5: '0'
    style6: '0'
    style7: '0'
    style8: '0'
    style9: '0'
    tr_chan: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_slope: qtgui.TRIG_SLOPE_POS
    tr_tag: '""'
    type: complex
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    xmax: '2'
    xmin: '-2'
    ymax: '2'
    ymin: '-2'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1056, 676.0]
    rotation: 0
    state: true
- name: qtgui_freq_sink_x_0_0
  id: qtgui_freq_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    average: '1.0'
    axislabels: 'True'
    bw: samp_rate
    color1: '"blue"'
    color10: '"dark blue"'
    color2: '"red"'
    color3: '"green"'
    color4: '"black"'
    color5: '"cyan"'
    color6: '"magenta"'
    color7: '"yellow"'
    color8: '"dark red"'
    color9: '"dark green"'
    comment: ''
    ctrlpanel: 'False'
    fc: '0'
    fftsize: 1024*8
    freqhalf: 'True'
    grid: 'False'
    gui_hint: ''
    label: Relative Gain
    label1: Receive Tuned
    label10: ''''''
    label2: ''''''
    label3: ''''''
    label4: ''''''
    label5: ''''''
    label6: ''''''
    label7: ''''''
    label8: ''''''
    label9: ''''''
    legend: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    name: '""'
    nconnections: '1'
    norm_window: 'False'
    showports: 'False'
    tr_chan: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_tag: '""'
    type: complex
    units: dB
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    wintype: window.WIN_BLACKMAN_hARRIS
    ymax: '10'
    ymin: '-140'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [952, 396.0]
    rotation: 0
    state: true
- name: qtgui_time_sink_x_0
  id: qtgui_time_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    axislabels: 'True'
    color1: blue
    color10: dark blue
    color2: red
    color3: green
    color4: black
    color5: cyan
    color6: magenta
    color7: yellow
    color8: dark red
    color9: dark green
    comment: ''
    ctrlpanel: 'False'
    entags: 'True'
    grid: 'False'
    gui_hint: ''
    label1: Signal 1
    label10: Signal 10
    label2: Signal 2
    label3: Signal 3
    label4: Signal 4
    label5: Signal 5
    label6: Signal 6
    label7: Signal 7
    label8: Signal 8
    label9: Signal 9
    legend: 'True'
    marker1: '0'
    marker10: '-1'
    marker2: '0'
    marker3: '-1'
    marker4: '-1'
    marker5: '-1'
    marker6: '-1'
    marker7: '-1'
    marker8: '-1'
    marker9: '-1'
    name: '""'
    nconnections: '1'
    size: 1024*8
    srate: samp_rate
    stemplot: 'False'
    style1: '1'
    style10: '1'
    style2: '1'
    style3: '1'
    style4: '1'
    style5: '1'
    style6: '1'
    style7: '1'
    style8: '1'
    style9: '1'
    tr_chan: '0'
    tr_delay: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_slope: qtgui.TRIG_SLOPE_POS
    tr_tag: '""'
    type: complex
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    ylabel: Amplitude
    ymax: '1'
    ymin: '-1'
    yunit: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [928, 92.0]
    rotation: 0
    state: true
- name: qtgui_time_sink_x_0_0_0
  id: qtgui_time_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    axislabels: 'True'
    color1: blue
    color10: dark blue
    color2: red
    color3: green
    color4: black
    color5: cyan
    color6: magenta
    color7: yellow
    color8: dark red
    color9: dark green
    comment: ''
    ctrlpanel: 'False'
    entags: 'True'
    grid: 'False'
    gui_hint: ''
    label1: Receive Tuned Re
    label10: Signal 10
    label2: Receive Tuned Im
    label3: Signal 3
    label4: Signal 4
    label5: Signal 5
    label6: Signal 6
    label7: Signal 7
    label8: Signal 8
    label9: Signal 9
    legend: 'True'
    marker1: '-1'
    marker10: '-1'
    marker2: '-1'
    marker3: '-1'
    marker4: '-1'
    marker5: '-1'
    marker6: '-1'
    marker7: '-1'
    marker8: '-1'
    marker9: '-1'
    name: '""'
    nconnections: '1'
    size: 1024*8
    srate: samp_rate
    stemplot: 'False'
    style1: '1'
    style10: '1'
    style2: '1'
    style3: '1'
    style4: '1'
    style5: '1'
    style6: '1'
    style7: '1'
    style8: '1'
    style9: '1'
    tr_chan: '0'
    tr_delay: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_slope: qtgui.TRIG_SLOPE_POS
    tr_tag: '""'
    type: complex
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    ylabel: Amplitude
    ymax: '1'
    ymin: '-1'
    yunit: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [960, 308.0]
    rotation: 0
    state: enabled
- name: qtgui_time_sink_x_0_0_0_0
  id: qtgui_time_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    axislabels: 'True'
    color1: blue
    color10: dark blue
    color2: red
    color3: green
    color4: black
    color5: cyan
    color6: magenta
    color7: yellow
    color8: dark red
    color9: dark green
    comment: ''
    ctrlpanel: 'False'
    entags: 'True'
    grid: 'False'
    gui_hint: ''
    label1: Receive^4 Re
    label10: Signal 10
    label2: Receive^4 Im
    label3: Signal 3
    label4: Signal 4
    label5: Signal 5
    label6: Signal 6
    label7: Signal 7
    label8: Signal 8
    label9: Signal 9
    legend: 'True'
    marker1: '-1'
    marker10: '-1'
    marker2: '-1'
    marker3: '-1'
    marker4: '-1'
    marker5: '-1'
    marker6: '-1'
    marker7: '-1'
    marker8: '-1'
    marker9: '-1'
    name: '""'
    nconnections: '1'
    size: 1024*8
    srate: samp_rate
    stemplot: 'False'
    style1: '1'
    style10: '1'
    style2: '1'
    style3: '1'
    style4: '1'
    style5: '1'
    style6: '1'
    style7: '1'
    style8: '1'
    style9: '1'
    tr_chan: '0'
    tr_delay: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_slope: qtgui.TRIG_SLOPE_POS
    tr_tag: '""'
    type: complex
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    ylabel: Amplitude
    ymax: '1'
    ymin: '-1'
    yunit: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1144, 212.0]
    rotation: 0
    state: enabled
- name: qtgui_time_sink_x_0_0_0_1
  id: qtgui_time_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    axislabels: 'True'
    color1: blue
    color10: dark blue
    color2: red
    color3: green
    color4: black
    color5: cyan
    color6: magenta
    color7: yellow
    color8: dark red
    color9: dark green
    comment: ''
    ctrlpanel: 'False'
    entags: 'True'
    grid: 'False'
    gui_hint: ''
    label1: Receive Costas Re
    label10: Signal 10
    label2: Receive Costas Im
    label3: Signal 3
    label4: Signal 4
    label5: Signal 5
    label6: Signal 6
    label7: Signal 7
    label8: Signal 8
    label9: Signal 9
    legend: 'True'
    marker1: '-1'
    marker10: '-1'
    marker2: '-1'
    marker3: '-1'
    marker4: '-1'
    marker5: '-1'
    marker6: '-1'
    marker7: '-1'
    marker8: '-1'
    marker9: '-1'
    name: '""'
    nconnections: '1'
    size: 1024*8
    srate: samp_rate
    stemplot: 'False'
    style1: '1'
    style10: '1'
    style2: '1'
    style3: '1'
    style4: '1'
    style5: '1'
    style6: '1'
    style7: '1'
    style8: '1'
    style9: '1'
    tr_chan: '0'
    tr_delay: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_slope: qtgui.TRIG_SLOPE_POS
    tr_tag: '""'
    type: complex
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    ylabel: Amplitude
    ymax: '1'
    ymin: '-1'
    yunit: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1080, 548.0]
    rotation: 0
    state: enabled
- name: qtgui_time_sink_x_1
  id: qtgui_time_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    axislabels: 'True'
    color1: blue
    color10: dark blue
    color2: red
    color3: green
    color4: black
    color5: cyan
    color6: magenta
    color7: yellow
    color8: dark red
    color9: dark green
    comment: ''
    ctrlpanel: 'False'
    entags: 'True'
    grid: 'False'
    gui_hint: ''
    label1: Decoded
    label10: Signal 10
    label2: Signal 2
    label3: Signal 3
    label4: Signal 4
    label5: Signal 5
    label6: Signal 6
    label7: Signal 7
    label8: Signal 8
    label9: Signal 9
    legend: 'True'
    marker1: '-1'
    marker10: '-1'
    marker2: '-1'
    marker3: '-1'
    marker4: '-1'
    marker5: '-1'
    marker6: '-1'
    marker7: '-1'
    marker8: '-1'
    marker9: '-1'
    name: '""'
    nconnections: '1'
    size: 1024*8
    srate: samp_rate
    stemplot: 'False'
    style1: '1'
    style10: '1'
    style2: '1'
    style3: '1'
    style4: '1'
    style5: '1'
    style6: '1'
    style7: '1'
    style8: '1'
    style9: '1'
    tr_chan: '0'
    tr_delay: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_slope: qtgui.TRIG_SLOPE_POS
    tr_tag: '""'
    type: float
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    ylabel: Amplitude
    ymax: '4'
    ymin: '-1'
    yunit: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1448, 668.0]
    rotation: 0
    state: true

connections:
- [analog_agc_xx_0, '0', blocks_exponentiate_const_cci_0, '0']
- [analog_noise_source_x_0, '0', blocks_add_xx_0, '0']
- [analog_random_uniform_source_x_0, '0', digital_chunks_to_symbols_xx_0, '0']
- [analog_sig_source_x_0, '0', blocks_multiply_xx_0, '1']
- [blocks_add_xx_0, '0', digital_costas_loop_cc_0, '0']
- [blocks_add_xx_0, '0', qtgui_const_sink_x_0_0, '0']
- [blocks_add_xx_0, '0', qtgui_freq_sink_x_0_0, '0']
- [blocks_add_xx_0, '0', qtgui_time_sink_x_0_0_0, '0']
- [blocks_char_to_float_0, '0', qtgui_time_sink_x_1, '0']
- [blocks_exponentiate_const_cci_0, '0', qtgui_time_sink_x_0_0_0_0, '0']
- [blocks_multiply_xx_0, '0', analog_agc_xx_0, '0']
- [blocks_multiply_xx_0, '0', blocks_add_xx_0, '1']
- [blocks_repeat_0, '0', blocks_multiply_xx_0, '0']
- [blocks_repeat_0, '0', qtgui_time_sink_x_0, '0']
- [blocks_vector_source_x_0, '0', digital_chunks_to_symbols_xx_0, '0']
- [digital_chunks_to_symbols_xx_0, '0', blocks_repeat_0, '0']
- [digital_constellation_decoder_cb_0, '0', blocks_char_to_float_0, '0']
- [digital_costas_loop_cc_0, '0', digital_constellation_decoder_cb_0, '0']
- [digital_costas_loop_cc_0, '0', qtgui_const_sink_x_0_0_0, '0']
- [digital_costas_loop_cc_0, '0', qtgui_time_sink_x_0_0_0_1, '0']

metadata:
  file_format: 1
  grc_version: 3.10.7.0
