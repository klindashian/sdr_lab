### Lesson 12. Phase-Shift Keying (PSK)

В примере 11 урока использовался вектор значений для модуляции амплитуды несущей волны, что позволило отправлять символы, которые могли содержать больше информации, чем один бит. В BPSK модуляции несущая волна умножается либо на 1, либо на -1. 

Для реализации как и в прошлых примерах, в качестве исходного сигнала используется `vector source` для предоставления строки значений, только на этот раз нашими входными данными будет строка двоичных цифр в шаблоне, который мы будем повторять снова и снова. Чтобы преобразовать этот список символов в значения 1 и –1, которые в дальнейшем будут использоваться для кодирования, используется блок `chunks to symbol`.

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson12/images/lesson12_1.jpeg" width="60%"/>
    <figcaption><center>Рисунок 1. Исходная схема </center></figcaption>
    </figure>
</div>

Результы работы схемы:

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson12/images/lesson12_2.jpeg" width="60%"/>
    <figcaption><center>Рисунок 2. Амплитуда принятого сигнала </center></figcaption>
    </figure>
</div>

Для визуализации работы схемы увеличим величину `frequency offset`

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson12/images/lesson12_4.jpeg" width="60%"/>
    <figcaption><center>Рисунок 3.1. Влияние смещения частоты </center></figcaption>
    </figure>
</div>

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson12/images/lesson12_3.jpeg" width="60%"/>
    <figcaption><center>Рисунок 3.2. Влияние смещения частоты </center></figcaption>
    </figure>
</div>

Аналогично предыдущим примерам в канал был добавлен шум, как показано на рисунке.

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson12/images/lesson12_noise.jpeg" width="60%"/>
    <figcaption><center>Рисунок 4. Добавление шума в канал </center></figcaption>
    </figure>
</div>

Результаты работы схемы под влиянием шума.

 <div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson12/images/lesson12_noise01.png" width="60%"/>
    <figcaption><center>Рисунок 4.1. Влияние шума мощностью 0.1 </center></figcaption>
    </figure>
</div>

<div align="center">
    <figure >
    <img src=" /Ex_GNURadio/Lesson12/images/lesson12_noise03.jpeg" width="60%"/>
    <figcaption><center>Рисунок 4.2. Влияние шума мощностью 0.3 </center></figcaption>
    </figure>
</div>