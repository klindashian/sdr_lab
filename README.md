# SDR_lab

Проект содержит лаюораторные работы по курсу "Прототипирование в телекоммуникационных системах на базе программируемой логической интегральной схемы и цифрового сигнального процессора"

- В директории [Ex_GNURadio](https://gitlab.com/klindashian/sdr_lab/-/tree/main/Ex_GNURadio?ref_type=heads) находятся краткие отчеты по изучению материалов [Learn SDR](https://gallicchio.github.io/learnSDR/)
- В директории [PySDR](https://gitlab.com/klindashian/sdr_lab/-/tree/main/PySDR?ref_type=heads) находятся ноутбуки по основным [главам](https://pysdr.org/content/intro.html) c дополнениями
- В директории [RFSoC_SDR](https://gitlab.com/klindashian/sdr_lab/-/tree/main/RFSoC_SDR?ref_type=heads) реализация ноутбуков `Quadrature Modulation & Complex Exponentials`